//
//  TTDGameTests.swift
//  TDD
//
//  Created by Dee Bitros on 7/10/16.
//  Copyright © 2016 Dionysios. All rights reserved.
//

import XCTest

@testable import TDD

class TTDGameTests: XCTestCase {
    
    let game = Game()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGameStartsAtZero() {
        XCTAssertTrue(game.score == 0)
    }
    
    func testIfMoveIsFizz() {
        game.score = 2
        let result = game.play(move:"Fizz")
        XCTAssertEqual(result, true)
    }
    
    func testIfMoveIsNotFizz() {
        game.score = 1
        let result = game.play(move:"Fizz")
        XCTAssertEqual(result, false)
    }
    
    func testIfMoveIsBuzz() {
        game.score = 4
        let result = game.play(move:"Buzz")
        XCTAssertEqual(result, true)
    }
    
    func testIfMoveIsNotBuzz() {
        game.score = 1
        let result = game.play(move:"Buzz")
        XCTAssertEqual(result, false)
    }
    
    func testIfMoveIsFizzBuzz() {
        game.score = 14
        let result = game.play(move:"FizzBuzz")
        XCTAssertEqual(result, true)
    }
    
    func testIfMoveIsNotFizzBuzz() {
        game.score = 2
        let result = game.play(move:"FizzBuzz")
        XCTAssertEqual(result, false)
    }
    
    func testIfMoveWrongScoreNotIncremented() {
        game.score = 1
        let _ = game.play(move: "Fizz")
        XCTAssertEqual(game.score, 1)
    }
}
