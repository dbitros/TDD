//
//  TDDTests.swift
//  TDDTests
//
//  Created by Dee Bitros on 7/10/16.
//  Copyright © 2016 Dionysios. All rights reserved.
//

import XCTest
@testable import TDD

class TDDTests: XCTestCase {
    
    let brain = Brain()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIsDivisibleByThree() {
        let result = brain.isDivisibleBy(divisor: 3,number: 3)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByThree() {
        let result = brain.isDivisibleBy(divisor: 3,number: 2)
        XCTAssertEqual(result, false)
    }
    
    func testIsDivisibleByFive() {
        let result = brain.isDivisibleBy(divisor: 5,number: 5)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByFive() {
        let result = brain.isDivisibleBy(divisor: 5,number: 3)
        XCTAssertEqual(result, false)
    }
    
    func testIsDivisibleByFifthteen() {
        let result = brain.isDivisibleBy(divisor: 15,number: 15)
        XCTAssertEqual(result, true)
    }
    
    func testIsNotDivisibleByFifthteen() {
        let result = brain.isDivisibleBy(divisor: 15,number: 2)
        XCTAssertEqual(result, false)
    }
    
    func testSayFizz() {
        let result = brain.check(check: 3)
        XCTAssertEqual(result, "Fizz")
    }
    
    func testSayBuzz() {
        let result = brain.check(check: 5)
        XCTAssertEqual(result, "Buzz")
    }
    
    func testSayFizzBuzz() {
        let result = brain.check(check: 15)
        XCTAssertEqual(result, "FizzBuzz")
    }
    
    func testCheck1() {
        let result = brain.check(check: 1)
        XCTAssertEqual(result, "1")
    }

    
    
}
