//
//  Brain.swift
//  TDD
//
//  Created by Dee Bitros on 7/10/16.
//  Copyright © 2016 Dionysios. All rights reserved.
//

import Foundation

class Brain:NSObject {
    
    func isDivisibleBy(divisor: Int, number: Int) -> Bool {
        return number % divisor == 0
    }
    
    func check(check:Int) -> String {
        
        if isDivisibleBy(divisor: 15, number: check) {
            return "FizzBuzz"
        } else if isDivisibleBy(divisor: 3, number: check) {
            return "Fizz"
        } else if isDivisibleBy(divisor: 5, number: check) {
            return "Buzz"
        } else {
            return "1"
        }
    }
    
    
}
