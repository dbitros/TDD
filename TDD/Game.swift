//
//  Game.swift
//  TDD
//
//  Created by Dee Bitros on 7/10/16.
//  Copyright © 2016 Dionysios. All rights reserved.
//

import Foundation

class Game:NSObject {
    
    var score:Int = 0
    var brain = Brain()
    
    override init() {
        score = 0
        brain = Brain()
        super.init()
    }
    
    func play(move:String) -> Bool {
    
        let result = brain.check(check: score + 1)
        
        if result == move {
            score += 1
            return true
        } else {
            return false
        }
        
    }
    
    func move(name:String) {
        score += 1
       
    }
}
